#ifndef __MY_STM32F4_UART_LIB_H
#define __MY_STM32F4_UART_LIB_H
#include "my_stm32f4_uart_driver.h"

typedef enum
{
    read = 0,
    write,
    readwrite
}UART_ComType;

typedef enum
{
    br_9600 = 0,
    br_115200
}UART_BaudrateType;

void LIB_UART_Init(UART_ComType comtype, UART_BaudrateType baudrate);
void LIB_UART_Write(char ch);
char LIB_UART_Read();

#endif
